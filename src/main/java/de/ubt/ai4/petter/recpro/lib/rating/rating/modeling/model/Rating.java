package de.ubt.ai4.petter.recpro.lib.rating.rating.modeling.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "ratingType", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = BinaryRating.class, name = "BINARY"),
        @JsonSubTypes.Type(value = ContinuousRating.class, name = "CONTINUOUS"),
        @JsonSubTypes.Type(value = IntervalBasedRating.class, name = "INTERVAL_BASED"),
        @JsonSubTypes.Type(value = OrdinalRating.class, name = "ORDINAL"),
        @JsonSubTypes.Type(value = UnaryRating.class, name = "UNARY")
})
public class Rating {
    private String id;
    private String name;
    private String description;
    private RatingType ratingType;
}
